import java.io.*;

public class AplicJ {
	
	public static void main(String args[]) throws IOException {

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Numarul natural a este:");
		String n = br.readLine();
		int a = Integer.parseInt(n);
		int b = 0;
		b = (int) Math.sqrt(a);

		if (a >= 0) {

			if (b * b == a) {

				System.out.println("Numarul " + a + " este patrat perfect.");
			} 
			else {
				System.out.println("Numarul " + a + " nu este patrat perfect.");
			}
		} 
		else {
			System.out.println("Numarul " + a + " nu este natural.");
		}
		
	}
}