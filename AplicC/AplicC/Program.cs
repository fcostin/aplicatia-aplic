﻿using System;

namespace Aplicatie
{
    class AplicC
    {
        static void Main(string[] args)
        {

            int a, b;
            string n;
            Console.WriteLine("Numarul natural a este: ");
            n = Console.ReadLine();
            a = int.Parse(n);
            b = 0;
            b = (int)Math.Sqrt(a);
            if (a >= 0)
                if (b * b == a)
                {
                    Console.WriteLine("Numarul " + a + " este patrat perfect.");
                }
                else
                {
                    Console.WriteLine("Numarul " + a + " nu este patrat perfect.");
                }
            else
            {
                Console.WriteLine("Numarul " + a + " nu este numar natural.");
            }
        }
    }
}
